import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Dish, YammyDishes } from './interfaces/dishes';
import { DISHES } from './mockdata/mock-dishes';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private messageService: MessageService) { }

  getDishes(): Observable<Dish[]> {
    const dishes = of(DISHES.dishes);
    this.messageService.add('DishService: fetched dishes');
    return dishes;
  }

}
