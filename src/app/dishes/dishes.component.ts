import { Component, OnInit } from '@angular/core';
import { Dish } from '../interfaces/dishes';
import { MessageService } from '../message.service';
import { DishService } from '../dish.service';

@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.scss']
})
export class DishesComponent implements OnInit {

  constructor(private dishService: DishService, private messageService: MessageService) { }

  selectedDish?: Dish;
  dishes : Dish[] = [];

  ngOnInit(): void {
    this.getDishes();
  }

  onSelect(dish: Dish): void {
    this.selectedDish = dish;
    this.messageService.add(`DishesComponent: Selected dish id=${dish.id}`);
  }

  getDishes(): void {
    this.dishService.getDishes()
        .subscribe(dishes => this.dishes = dishes);
  }

}
