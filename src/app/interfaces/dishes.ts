
export interface Dish {
    id: number;
    title: string;
    description: string;
    season: string;
    dateAdded: string;
    imageBytes: string;
    imageUrl: string;
    imagePath: string;
  }

  export interface YammyDishes {
    dishes: Dish[];
  }
