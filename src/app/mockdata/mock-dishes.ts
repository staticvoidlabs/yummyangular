import { Dish, YammyDishes } from '../interfaces/dishes';

export const DISHES: YammyDishes = 
  { dishes: [
  { id: 1, title: 'Dr Nice', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 2, title: 'Narco', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 3, title: 'Bombasto', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 4, title: 'Celeritas', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 5, title: 'Magneta', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 6, title: 'RubberMan', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 7, title: 'Dynama', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 8, title: 'Dr IQ', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 9, title: 'Magma', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' },
  { id: 10, title: 'Tornado', season: 'sw', dateAdded: '20210715_10:25', imageBytes: 'xxx', imageUrl: 'xxx', imagePath: 'xxx' }
]};

/*
export const DISHES: Dish[] = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
*/